(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let pp_bytes ppf b =
  let `Hex h = Hex.of_bytes b in
  Format.fprintf ppf "0x%s" h

let random_bytes len rng =
  let open Random.State in
  let l = len rng in
  Bytes.init l (fun _ -> Char.chr (int rng 256))

module LSL = struct
  open Bytes

  let reference bs n =
    let open Bytes in
    let nbytes = n / 8 in
    let nbits = n mod 8 in
    if nbits = 0 then cat bs @@ make nbytes '\000'
    else
      cat (init (length bs + 1) (fun i ->
          let c1 = if i = 0 then 0 else Char.code @@ get bs (i-1) in
          let c2 = if i = length bs then 0 else Char.code @@ get bs i in
          Char.chr ((c1 lsl 8 + c2) lsr (8 - nbits) land 255)))
      @@ make nbytes '\000'


  (*
        <-------------len_a------------------->
                                       <-rest->
    a  |<-----------------------------|------->|
       |            x8                |                   <-nbytes->
    r  |0..0|<------------------------|----|------->|0..0|0000000000|
                                       <--  rest + 1  -->
  *)
  let shift_left a n =
    let len_a = length a in
    let nbytes = n / 8 in
    let nbits = n mod 8 in
    if nbits = 0 then cat a @@ make nbytes '\000'
    else
      let nbits' = 8 - nbits in
      let r = create (len_a + 1 + nbytes) in
      let upto = len_a / 8 in
      (* x8 part *)
      let rec loop prev i =
        if i = upto then prev
        else
          let i8 = i * 8 in
          let here = get_int64_be a i8 in
          set_int64_be r i8
            (Int64.add
               (Int64.shift_left prev (64-nbits'))
               (Int64.shift_right_logical here nbits'));
          loop here (i+1)
      in
      let prev = loop 0L 0 in
      (* rest + 1 part *)
      let prev = Int64.to_int prev land 255 in
      let rec loop prev i =
        if i = len_a then prev
        else
          let here = Char.code @@ unsafe_get a i in
          let c = Char.chr (((prev lsl 8 + here) lsr nbits') land 255) in
          unsafe_set r i c;
          loop here (i+1)
      in
      let prev = loop prev (upto * 8) in
      unsafe_set r len_a (Char.chr ((prev lsl nbits) land 255));
      (* nbytes part *)
      unsafe_fill r (len_a+1) nbytes '\000';
      r

  let (!) = Bytes.unsafe_of_string

  let () =
    let test a n b =
      if shift_left !a n <> !b then begin
        Format.eprintf "%a %d@.%a@.%a@." pp_bytes !a n pp_bytes !b pp_bytes (shift_left !a n);
        assert false
      end
    in
    test "\012\123" 0 "\012\123";
    test "\012\123" 8 "\012\123\000";
    test "\012\123" 16 "\012\123\000\000";
    test "\012\123" 1 "\000\024\246";
    test "\012\123" 9 "\000\024\246\000";
    test "\012\123" 17 "\000\024\246\000\000";
    test "\012\123" 2 "\000\049\236";
    test "\012\123" 3 "\000\099\216";
    test "\012\123" 4 "\000\199\176";
    test "\012\123" 5 "\001\143\096";
    test "\012\123" 6 "\003\030\192";
    test "\012\123" 7 "\006\061\128";

    test "\001\001\001\001\001\001\001" 0 "\001\001\001\001\001\001\001";
    test "\001\001\001\001\001\001\001" 1 "\000\002\002\002\002\002\002\002";
    test "\001\001\001\001\001\001\001\001" 0 "\001\001\001\001\001\001\001\001";
    test "\001\001\001\001\001\001\001\001" 1 "\000\002\002\002\002\002\002\002\002";
    test "\001\001\001\001\001\001\001\001\001" 0 "\001\001\001\001\001\001\001\001\001";
    test "\001\001\001\001\001\001\001\001\001" 1 "\000\002\002\002\002\002\002\002\002\002";
    test "\001\001\001\001\001\001\001\001\002\002\002\002\002\002\002\002\255" 1
     "\000\002\002\002\002\002\002\002\002\004\004\004\004\004\004\004\005\254";

    test "" 0 "";
    test "" 8 "\000";
    test "" 9 "\000\000"

  let test () =
    let rng = Stdlib.Random.State.make [||] in
    let sz = 30 in
    for _ = 0 to 10000 do
      let a = random_bytes (fun rng -> Stdlib.Random.State.int rng sz) rng in
      let n = Stdlib.Random.State.int rng 256 in
      let s = shift_left a n in
      let r = reference a n in
      if s <> r then begin
        Format.eprintf "a %a %d@.s %a@.r %a@."
          pp_bytes a n
          pp_bytes s
          pp_bytes r;
        assert false
      end
    done

  let () = test ()
  let () = prerr_endline "shift_left tested"

  let bench sz f =
    let rng = Stdlib.Random.State.make [||] in
    let a = random_bytes (fun _ -> sz) rng in
    let n = Stdlib.Random.State.int rng 256 in
    fun () -> ignore @@ f a n

  let () =
    let open Core in
    let open Core_bench in
    let sizes = [10;999;1000;99999] in
    Command_unix.run (Bench.make_command [
      Bench.Test.create_indexed ~name:"lsl_ref"
        ~args: sizes
        (fun sz -> Staged.stage (bench sz reference));
      Bench.Test.create_indexed ~name:"lsl_int64"
        ~args: sizes
        (fun sz -> Staged.stage (bench sz shift_left));
    ])
end

module LSR = struct
  open Bytes

  let reference bs n =
    let open Bytes in
    let nbytes = n / 8 in
    let nbits = n mod 8 in
    if nbits = 0 then sub bs 0 (max 0 (length bs - nbytes))
    else
      init (max 0 (length bs - nbytes)) (fun i ->
          let c1 = if i = 0 then 0 else Char.code @@ get bs (i-1) in
          let c2 = Char.code @@ get bs i in
          Char.chr ((c1 lsl 8 + c2) lsr nbits land 255))

  (*
        <-------------len_a------------------->
                           <-rest->
    a  |<-----------------|--------|---------->|
       |   \        x8     \       |
    r  |0..0|<------------|--|-----|

       <----------- len ----------> <- nbytes->

       <----- part I ---->
  *)
  let shift_right a n =
    let len_a = length a in
    let nbytes = n / 8 in
    let nbits = n mod 8 in
    let len = max 0 (len_a - nbytes) in
    if nbits = 0 then sub a 0 len
    else
      let r = create len in
      let upto = len / 8 in
      (* part I *)
      let rec loop prev i =
        if i = upto then prev
        else
          let i8 = i * 8 in
          let here = get_int64_be a i8 in
(*
          Format.eprintf "i8 %d  prev %08Lx  here %08Lx@." i8 prev here;
          Format.eprintf "i8 %d  prev< %016Lx  here> %016Lx@." i8 (Int64.shift_left prev (64-nbits)) (Int64.shift_right_logical here nbits);
          Format.eprintf "ADD  %Lx@."
            (Int64.add
               (Int64.shift_left prev (64-nbits))
               (Int64.shift_right_logical here nbits));
*)
          set_int64_be r i8
            (Int64.add
               (Int64.shift_left prev (64-nbits))
               (Int64.shift_right_logical here nbits));
          loop here (i+1)
      in
      (* rest part *)
      let prev = loop 0L 0 in
      let prev = 255 land (Int64.to_int prev) in
      let rec loop prev i =
        if i = len then ()
        else
          let here = Char.code @@ unsafe_get a i in
          unsafe_set r i (Char.chr ( (prev lsl (8-nbits) + here lsr nbits) land 255));
          loop here (i+1)
      in
      loop prev (upto*8);
      r

  let (!) = Bytes.unsafe_of_string

  let () =
    let test a n b =
      if shift_right !a n <> !b then begin
        Format.eprintf "%a %d@.%a@.%a@." pp_bytes !a n pp_bytes !b pp_bytes (shift_right !a n);
        assert false
      end
    in
    test "\012\123" 0 "\012\123";
    test "\012\123" 8 "\012";
    test "\012" 8 "";
    test "\012" 16 "";
    test "\012" 9 "";
    test "" 0 "";
    test "" 1 "";
    test "" 8 "";
    test "" 9 "";

    test "\012\123" 1 "\006\061";
    test "\012\123\099" 9 "\006\061";

    test "\012\123" 2 "\003\030";
    test "\012\123" 3 "\001\143";
    test "\012\123" 4 "\000\199";
    test "\012\123" 5 "\000\099";
    test "\012\123" 6 "\000\049";
    test "\012\123" 7 "\000\024";

    test "\128\128\128\128\128\128\128\128\064\064\064\064\064\064\064\064\032\032" 0
         "\128\128\128\128\128\128\128\128\064\064\064\064\064\064\064\064\032\032";
    test "\128\128\128\128\128\128\128\128\064\064\064\064\064\064\064\064\032\032" 1
         "\064\064\064\064\064\064\064\064\032\032\032\032\032\032\032\032\016\016";
    test "\128\128\128\128\128\128\128\128\064\064\064\064\064\064\064\064\032\032" 2
         "\032\032\032\032\032\032\032\032\016\016\016\016\016\016\016\016\008\008";
    test "\128\128\128\128\128\128\128\128\064\064\064\064\064\064\064\064\032\032" 7
         "\001\001\001\001\001\001\001\001\000\128\128\128\128\128\128\128\128\064";
    test "\128\128\128\128\128\128\128\128\064\064\064\064\064\064\064\064\032\032" 9
             "\064\064\064\064\064\064\064\064\032\032\032\032\032\032\032\032\016";
    ()

  let test () =
    let rng = Stdlib.Random.State.make [||] in
    let sz = 30 in
    for _ = 0 to 10000 do
      let a = random_bytes (fun rng -> Stdlib.Random.State.int rng sz) rng in
      let n = Stdlib.Random.State.int rng 40 in
      let s = shift_right a n in
      let r = reference a n in
      if s <> r then begin
        Format.eprintf "a %a %d@.s %a@.r %a@."
          pp_bytes a n
          pp_bytes s
          pp_bytes r;
        assert false
      end
    done

  let () = test ()

  let () = prerr_endline "shift_right tested"

  let bench sz f =
    let rng = Stdlib.Random.State.make [||] in
    let a = random_bytes (fun _ -> sz) rng in
    let n = Stdlib.Random.State.int rng 256 in
    fun () -> ignore @@ f a n

  let () =
    let open Core in
    let open Core_bench in
    let sizes = [10;999;1000;99999] in
    Command_unix.run (Bench.make_command [
      Bench.Test.create_indexed ~name:"lsr_ref"
        ~args: sizes
        (fun sz -> Staged.stage (bench sz reference));
      Bench.Test.create_indexed ~name:"lsr_int64"
        ~args: sizes
        (fun sz -> Staged.stage (bench sz shift_right));
    ])
end

module Reference = struct
  (* Reference implementation *)

  open Bytes

  let bytes_logical_binop f a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let a = if len_a < len then cat (make (len - len_a) '\000') a else a in
    let b = if len_b < len then cat (make (len - len_b) '\000') b else b in
    init len @@ fun i ->
    Char.chr (f (Char.code @@ get a i) (Char.code @@ get b i))

  let bytes_or = bytes_logical_binop (lor)
  let bytes_and = bytes_logical_binop (land)
  let bytes_xor = bytes_logical_binop (lxor)
end

module Unsafe = struct
  (* Use unsafe functions without the boundary check *)

  open Bytes

  let get = unsafe_get
  let set = unsafe_set

  let bytes_logical_binop f a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let a = if len_a < len then cat (make (len - len_a) '\000') a else a in
    let b = if len_b < len then cat (make (len - len_b) '\000') b else b in
    init len @@ fun i ->
    Char.chr (f (Char.code @@ get a i) (Char.code @@ get b i))

  let bytes_or = bytes_logical_binop (lor)
  let bytes_and = bytes_logical_binop (land)
  let bytes_xor = bytes_logical_binop (lxor)
end

module UnsafeInst = struct
  (* Instantiate f parameter *)

  open Bytes

  let get = unsafe_get
  let set = unsafe_set

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let a = if len_a < len then cat (make (len - len_a) '\000') a else a in
    let b = if len_b < len then cat (make (len - len_b) '\000') b else b in
    init len @@ fun i ->
    Char.chr ((land)
                (Char.code @@ get a i)
                (Char.code @@ get b i))
end

module UnsafeInst2 = struct
  (* Not using the higher order function init *)

  open Bytes

  let get = unsafe_get
  let set = unsafe_set

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let a = if len_a < len then cat (make (len - len_a) '\000') a else a in
    let b = if len_b < len then cat (make (len - len_b) '\000') b else b in
    let r = create len in
    for i = 0 to len-1 do
      set r i @@ Char.chr ((land)
                             (Char.code @@ get a i)
                             (Char.code @@ get b i))
    done;
    r
end

module UnsafeInst3 = struct
  (* Not 0-pad an input *)

  open Bytes

  let get = unsafe_get
  let set = unsafe_set

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let open Bytes in
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let r = Bytes.create len in
    let diff_a = len - len_a in
    let diff_b = len - len_b in
    let pos = abs (len_a - len_b) in
    for i = 0 to pos - 1 do
      set r i '\000'
    done;
    for i = pos to len-1 do
      set r i @@ Char.chr ((land)
                             (Char.code @@ get a (i-diff_a))
                             (Char.code @@ get b (i-diff_b)))
    done;
    r
end

module UnsafeInst32 = struct
  (* UnsafeInst3 but using no subtraction but increments *)

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let open Bytes in
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let r = Bytes.create len in
    let diff_a = len - len_a in
    let diff_b = len - len_b in
    let pos = abs (len_a - len_b) in
    for i = 0 to pos - 1 do
      unsafe_set r i '\000'
    done;
    let pos_a = ref (pos - diff_a) in
    let pos_b = ref (pos - diff_b) in
    for i = pos to len-1 do
      unsafe_set r i @@ Char.chr ((land)
                                    (Char.code @@ unsafe_get a !pos_a)
                                    (Char.code @@ unsafe_get b !pos_b));
      incr pos_a;
      incr pos_b
    done;
    r
end

module UnsafeInst4 = struct
  (* UnsafeInst, but no bytes extension *)

  open Bytes

  let get = unsafe_get
  let set = unsafe_set

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let open Bytes in
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let diff = abs (len_a - len_b) in
    let diff_a = len - len_a in
    let diff_b = len - len_b in
    let r = create len in
    for i = 0 to len-1 do
      set r i
        (Char.chr (
            if i < diff then 0
            else
              let ca = Char.code @@ get a (i-diff_a) in
              let cb = Char.code @@ get b (i-diff_b) in
              ca land cb
          ))
    done;
    r
end

module C = struct
  external bytes_and : bytes -> bytes -> bytes = "bytes_land"
end

module ByZ = struct
  open Bytes

  let to_bytes len z =
    let b = unsafe_of_string @@ Z.to_bits z in
    let len_b = length b in
    match Int.compare len len_b with
    | 0 -> b
    | 1 -> cat b (make (len - len_b) '\000')
    | _ ->
        assert (sub b len (len_b - len) = Bytes.make (len_b - len) '\000');
        sub b 0 len

  let bytes_and a b =
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    (* Our bytes are in BE, but Z's bit representation is in LE.  Tricky! *)
    (*
       Longer   |<------------------------>|
       Shorter        |<------------------>|
    *)
    let za = Z.of_bits @@ Bytes.unsafe_to_string a in
    let zb = Z.of_bits @@ Bytes.unsafe_to_string b in
    let za = if len = len_a then za else Z.shift_left za ((len - len_a) * 8) in
    let zb = if len = len_b then zb else Z.shift_left zb ((len - len_b) * 8) in
    (*
       Longer   |<------------------------>|
       Shorter  |00000|<------------------>|
    *)
    let z = Z.(land) za zb in
    (*
       Longer   |<------------------------>|
       Shorter  |00000|<------------------>|

       Result   |00000|<------------------>|000000|  may have some extra 0s
       Result   |00000|<--------------->|            or less
    *)
    to_bytes len z
end

module ByInt64 = struct

  (*

              <-------------len_a------------------->
              <--diff-> <----------len_b------------>
                                             <-rest->
    Longer   |<-----------------------------|------->|
                       |          x8        |
    Shorter            |<-------------------|------->|
                       |          x8        |
    Result   |000000000|<-------------------|------->|
  *)

  open Bytes

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let diff = len_a - len_b in

    (* a is longer or equal *)
    let a, b, len_a, len_b, diff =
      if diff >= 0 then a, b, len_a, len_b, diff
      else b, a, len_b, len_a, - diff
    in
    let rest = len_b mod 8 in

    let r = create len_a in

    (* 0 part *)
    unsafe_fill r 0 diff '\000';

    (* x8 part *)
    for i = 0 to len_b / 8 - 1 do
      let i8 = i * 8 in
      let i8_diff = i8 + diff in
      set_int64_ne r i8_diff (Int64.logand (get_int64_ne a i8_diff) (get_int64_ne b i8))
    done;

    (* rest *)
    if rest <> 0 then begin
      let a' = create 8 in
      unsafe_blit a (len_a - rest) a' 0 rest;
      let b' = create 8 in
      unsafe_blit b (len_b - rest) b' 0 rest;
      (* overwrite to a' to avoid an allocation *)
      set_int64_ne a' 0 (Int64.logand (get_int64_ne a' 0) (get_int64_ne b' 0));
      unsafe_blit a' 0 r (len_a-rest) rest
    end;

    r
end

module ByInt64_2 = struct

  (*

              <-------------len_a------------------->
              <--diff-> <----------len_b------------>
                                             <-rest->
    Longer   |<-----------------------------|------->|
                       |          x8        |
    Shorter            |<-------------------|------->|
                       |          x8        |
    Result   |000000000|<-------------------|------->|
  *)

  open Bytes

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let diff = len_a - len_b in

    (* a is longer or equal *)
    let a, b, len_a, len_b, diff =
      if diff >= 0 then a, b, len_a, len_b, diff
      else b, a, len_b, len_a, - diff
    in
    let rest = len_b mod 8 in

    let r = create len_a in

    (* 0 part *)
    unsafe_fill r 0 diff '\000';

    (* x8 part *)
    for i = 0 to len_b / 8 - 1 do
      let i8 = i * 8 in
      let i8_diff = i8 + diff in
      set_int64_ne r i8_diff (Int64.logand (get_int64_ne a i8_diff) (get_int64_ne b i8))
    done;

    (* rest *)
    for i = len_b - rest to len_b - 1 do
      (* unsafe... *)
      let i_diff = i + diff in
      let ca = Char.code @@ unsafe_get a i_diff in
      let cb = Char.code @@ unsafe_get b i in
      unsafe_set r i_diff (Char.chr (ca land cb))
    done;

    r
end

module ByInt64_2_safe = struct

  (*

              <-------------len_a------------------->
              <--diff-> <----------len_b------------>
                                             <-rest->
    Longer   |<-----------------------------|------->|
                       |          x8        |
    Shorter            |<-------------------|------->|
                       |          x8        |
    Result   |000000000|<-------------------|------->|
  *)

  open Bytes

  let bytes_and a b =
    (* The shorter bytes is left 0 padded *)
    let len_a = length a in
    let len_b = length b in
    let diff = len_a - len_b in

    (* a is longer or equal *)
    let a, b, len_a, len_b, diff =
      if diff >= 0 then a, b, len_a, len_b, diff
      else b, a, len_b, len_a, - diff
    in
    let rest = len_b mod 8 in

    let r = create len_a in

    (* 0 part *)
    fill r 0 diff '\000';

    (* x8 part *)
    for i = 0 to len_b / 8 - 1 do
      let i8 = i * 8 in
      let i8_diff = i8 + diff in
      set_int64_ne r i8_diff (Int64.logand (get_int64_ne a i8_diff) (get_int64_ne b i8))
    done;

    (* rest *)
    for i = len_b - rest to len_b - 1 do
      let i_diff = i + diff in
      let ca = Char.code @@ get a i_diff in
      let cb = Char.code @@ get b i in
      set r i_diff (Char.chr (ca land cb))
    done;

    r
end

let test n f =
  prerr_endline n;
  let rng = Stdlib.Random.State.make [||] in
  let sz = 100 in
  for _ = 0 to 10000 do
    let b1 = random_bytes (fun rng -> Stdlib.Random.State.int rng sz) rng in
    let b2 = random_bytes (fun rng -> Stdlib.Random.State.int rng sz) rng in
    if not (f b1 b2 = Reference.bytes_and b1 b2) then begin
      Format.eprintf "a %a@.b %a@.f %a@.r %a@." pp_bytes b1 pp_bytes b2 pp_bytes (f b1 b2) pp_bytes (Reference.bytes_and b1 b2);
      assert false
    end
  done

let () =
  test "unsafe" Unsafe.bytes_and;
  test "unsafeinst" UnsafeInst.bytes_and;
  test "unsafeinst2" UnsafeInst2.bytes_and;
  test "unsafeinst3" UnsafeInst3.bytes_and;
  test "unsafeinst32" UnsafeInst32.bytes_and;
  test "unsafeinst4" UnsafeInst4.bytes_and;
  test "c" C.bytes_and;
  test "z_and_bytes" ByZ.bytes_and;
  test "int64" ByInt64.bytes_and;
  test "int64_2" ByInt64_2.bytes_and;
  test "int64_2_safe" ByInt64_2_safe.bytes_and;
  ()

let bench sz f =
  let rng = Stdlib.Random.State.make [||] in
  let b1 = random_bytes (fun _ -> sz) rng in
  let b2 = random_bytes (fun _ -> sz) rng in
  fun () -> ignore @@ f b1 b2

let z sz =
  let rng = Stdlib.Random.State.make [||] in
  let b1 = random_bytes (fun _ -> sz) rng in
  let b2 = random_bytes (fun _ -> sz) rng in
  let z1 = Z.of_bits @@ Bytes.to_string b1 in
  let z2 = Z.of_bits @@ Bytes.to_string b2 in
  fun () -> ignore @@ Z.(z1 land z2)

let () =
  let open Core in
  let open Core_bench in
  let sizes = [10;999;1000;99999] in
  Command_unix.run (Bench.make_command [
    Bench.Test.create_indexed ~name:"ref"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz Reference.bytes_and));

(*
    Bench.Test.create_indexed ~name:"unsafe"
      ~args: [1000]
      (fun sz -> Staged.stage (bench sz Unsafe.bytes_and));

    Bench.Test.create_indexed ~name:"unsafeinst"
      ~args: [1000]
      (fun sz -> Staged.stage (bench sz UnsafeInst.bytes_and));
*)

    Bench.Test.create_indexed ~name:"unsafeinst2"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz UnsafeInst2.bytes_and));

(*
    Bench.Test.create_indexed ~name:"unsafeinst3"
      ~args: [1000]
      (fun sz -> Staged.stage (bench sz UnsafeInst3.bytes_and));

    Bench.Test.create_indexed ~name:"unsafeinst32"
      ~args: [1000]
      (fun sz -> Staged.stage (bench sz UnsafeInst32.bytes_and));

    Bench.Test.create_indexed ~name:"unsafeinst4"
      ~args: [1000]
      (fun sz -> Staged.stage (bench sz UnsafeInst4.bytes_and));
*)

    Bench.Test.create_indexed ~name:"c"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz C.bytes_and));

    Bench.Test.create_indexed ~name:"z_and_bytes"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz ByZ.bytes_and));

    Bench.Test.create_indexed ~name:"z_and_nat"
      ~args: sizes
      (fun sz -> Staged.stage (z sz));

    Bench.Test.create_indexed ~name:"int64"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz ByInt64.bytes_and));

    Bench.Test.create_indexed ~name:"int64_2"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz ByInt64_2.bytes_and));

    Bench.Test.create_indexed ~name:"int64_2_safe"
      ~args: sizes
      (fun sz -> Staged.stage (bench sz ByInt64_2_safe.bytes_and));
  ])


module I32 = struct
  (* 1000 0.139709 *)
  let get_i32 b =
    let open Bytes in
    let get b i = Char.code @@ get b i in
    function
    | -3 ->
      get b 0
    | -2 ->
      get b 0 lsl 8
      + get b 1
    | -1 ->
      get b 0 lsl 16
      + get b 1 lsl 8
      + get b 2
    | i ->
        get b i lsl 24
        + get b (i+1) lsl 16
        + get b (i+2) lsl 8
        + get b (i+3)

  let set_i32 b n =
    let open Bytes in
    let set b i n = set b i (Char.chr n) in
    function
    | -3 ->
        set b 0 (n land 255)
    | -2 ->
        set b 0 (n lsr 8 land 255);
        set b 1 (n land 255)
    | -1 ->
        set b 0 (n lsr 16 land 255);
        set b 1 (n lsr 8 land 255);
        set b 2 (n land 255);
    | i ->
        set b i (n lsr 24 land 255);
        set b (i+1) (n lsr 16 land 255);
        set b (i+2) (n lsr 8 land 255);
        set b (i+3) (n land 255)

  let bytes_logical_binop op a b =
    (* The shorter bytes is left 0 padded *)
    let open Bytes in
    let len_a = length a in
    let len_b = length b in
    let len = max len_a len_b in
    let a = if len_a < len then cat (make (len - len_a) '\000') a else a in
    let b = if len_b < len then cat (make (len - len_b) '\000') b else b in
    let x = create len in
    let len' = (len + 3) / 4 * 4 in
    let rec f i =
      if i >= len then ()
      else begin
        let ia = get_i32 a i in
        let ib = get_i32 b i in
        set_i32 x (op ia ib) i;
        f (i+4)
      end
    in
    f (len - len');
    x

  let bytes_or = bytes_logical_binop (lor)
  let bytes_and = bytes_logical_binop (land)
  let bytes_xor = bytes_logical_binop (lxor)
end
