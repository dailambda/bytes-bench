#include <strings.h>
#include "caml/mlvalues.h"
#include "caml/memory.h"
#include "caml/alloc.h"

CAMLprim value bytes_land(value a, value b)
{
    CAMLparam2(a, b);
    CAMLlocal1(r);
    mlsize_t la = caml_string_length(a);
    mlsize_t lb = caml_string_length(b);
    mlsize_t l = la > lb ? la : lb;
    int diff = abs((int)la - (int)lb);
 
   r = caml_alloc_string(l);

    unsigned char *pa = Bytes_val(a);
    unsigned char *pb = Bytes_val(b);
    unsigned char *pr = Bytes_val(r);

    bzero(pr, diff);

    pr += diff;
    pa = l == la ? pa + diff : pa;
    pb = l == lb ? pb + diff : pb;

    for(int i = diff; i < l; i++){
        *pr++ = *pa++ & *pb++;
    }
    CAMLreturn(r);
}

CAMLprim value bytes_lsl(value b, value vn)
{
    CAMLparam2(b, vn);
    int n = Int_val(vn);

    if (n == 0) {

        CAMLreturn(b);

    } else {
        int shift = n % 8;
        
        CAMLlocal1(r);

        if ( shift == 0 ) {

            mlsize_t len_b = caml_string_length(b);
            mlsize_t len_r = len_b + n / 8;

            r = caml_alloc_string(len_r);

            unsigned char *pb = Bytes_val(b);
            unsigned char *pr = Bytes_val(r);

            for(int i = 0; i < len_b; i++){
                *pr++ = *pb++;
            }
            bzero(pr, len_r - len_b);
        } else {
            mlsize_t len_b = caml_string_length(b);
            mlsize_t len_r = len_b + n / 8 + 1;

            r = caml_alloc_string(len_r);

            unsigned char *pb = Bytes_val(b);
            unsigned char *pr = Bytes_val(r);

            *pr++ = *pb >> (8 - shift);

            for(int i = 1; i < len_b; i++){
                *pr++ = (( (*pb << 8) + *(pb+1)) << shift) >> 8;
                pb++;
            }

            *pr++ = (*pb << shift) & 255; 

            bzero(pr, len_r - len_b - 1);
        }

        CAMLreturn(r);
    }
}


